

virtualenv -p python2 venv
. venv/bin/activate

# install dependencies via pip
pip install cython
pip install pygments
pip install sphinx
pip install pyglet
pip install pillow # Because PIL is utter crap
#pip install pygame  # Fails on Arch Linux

# Patch and install pygame
wget http://www.pygame.org/ftp/pygame-1.9.1release.tar.gz
wget https://projects.archlinux.org/svntogit/packages.git/plain/trunk/pygame-v4l.patch?h=packages/python-pygame -O pygame-v4l.patch
tar xf pygame-1.9.1release.tar.gz
cd pygame-1.9.1release/
patch -Np1 -i ../pygame-v4l.patch
python setup.py install
cd ..
rm pygame* -r

# The default setup.py is currently broken
#pip install git+git://github.com/kivy/kivy.git
pip install git+https://github.com/buchuki/kivy.git@setupfix




